'''
Script to load an inception V3 model from a pb file and perform inference till the desired operation (before or after the last fully connected).
'''
import os
import numpy as np
import tensorflow as tf
import pandas as pd
from tensorflow.core.framework import graph_pb2
from tensorflow.python.platform import gfile
from PIL import Image
from time import time
from multiprocessing import Pool
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--model_path", required=True, help='Path to the model pb file, e.g. /lustre/MHPC17/pierocor/thesis/models/inception_v3_frozen_train-on-nffa_lr0.01-exp-dec_wd0.0001_ep10_bs32.pb')
parser.add_argument("--dataset_dir", required=True, help='Path to images_flat')
parser.add_argument("--dataset_table", required=True, help='csv describing images_flat: /lustre/MHPC17/pierocor/nffa-dr/images_flat_script/images_flat.csv')
parser.add_argument("--output_file", required=True, help='CSV where to store the results')
parser.add_argument("--n_feat", required=True, help='Number of features (1001 or 2048)')
args = parser.parse_args()


model_path = args.model_path
dataset_dir = args.dataset_dir
dataset_table = args.dataset_table
output_file = args.output_file
n_outputs = int(args.n_feat)



batch_size = 150
gpu_count = 1   # Number of gpu to use for inference (0 or 1)
tensor = {1001: 'import/InceptionV3/Logits/SpatialSqueeze:0',\
         2048: 'import/InceptionV3/Logits/Dropout_1b/Identity:0'}



# Load table in a dataframe
images_flat = pd.read_csv(dataset_table, sep='\t')
# Consider a sub dataframe
X = images_flat.loc[(images_flat['scale_unit'] == 'μm') & \
                     ( (images_flat['scale_digit'] == 1) | (images_flat['scale_digit'] == 2) )]
# Forget about previous indices
images_flat.index = range(len(images_flat)) 
# Create a list of filenames from the dataframe
filenames = list(dataset_dir + images_flat['subdir'] + '/' + images_flat['hash_tif'] + '.jpg')

# Create a list of filenames batches
filenames_batches = [filenames[i:i + batch_size] for i in range(0, len(filenames), batch_size)]

print("Starting inference on", len(filenames),"images with ", gpu_count, " GPUs.")
start = time()

graph_def = graph_pb2.GraphDef()
with open(model_path, "rb") as f:
    graph_def.ParseFromString(f.read())
tf.import_graph_def(graph_def)

im_feat = np.zeros((len(filenames), n_outputs))
for j, batch in enumerate(filenames_batches):
    # Define a numpy array containing the whole batch resized to (299, 299) and normalized
    resized_images = np.zeros((len(batch),299,299,3))
    for i,image in enumerate(batch):
       
        img = Image.open(image)
        img = img.resize((299, 299))
        imgnumpy = np.ndarray((1,299,299,3))
        resized_images[i] = img
        
    resized_images /= 255
    # Start a tf.Session with the desired num of gpus and feed the graph with resized_images
    with tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=1, device_count={'GPU': gpu_count})) as sess:
        my_tensor = sess.graph.get_tensor_by_name(tensor[n_outputs])
        output = sess.run(my_tensor, feed_dict={'import/input:0': resized_images})
        
    im_feat[j*batch_size:(j+1)*batch_size] = np.squeeze(output)

end = time()    
print("feat_Logits of ", len(filenames), "images computed in: ",end - start)

# Join the results to the dataframe (Pay attention on the indices)
df = pd.DataFrame(im_feat)
images_flat = images_flat.join(df)
# Store the table
images_flat.to_csv(output_file, sep= '\t', index=False)