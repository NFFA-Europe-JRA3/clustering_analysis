import tensorflow as tf
import numpy as np
from time import time
from shutil import copytree
from datetime import datetime
import pickle


def neuron_layer(X, weights, bias, name, activation=None):
    '''
    Defines a fully connected layer
    X: 2D tensor (?,n_inputs)
    weights: 2D tensor (n_inputs, n_outputs)
    bias: 1D tensor (n_outputs)
    name: string
    activation: an activation function
    '''
    with tf.name_scope(name):
        n_inputs = int(X.get_shape()[1])
        Z = tf.matmul(X, weights) + bias
        if activation is not None:
            Z = activation(Z)
        return tf.identity(Z, 'output')

class autoencoder_3MPL:
    error1 = 'Train the autoencoder or load the weights before using this method.'
    error2 = 'Model already trained. Create a new object to load other weights.'
    def __init__(self, n_inputs,\
                 n_hidden = [100, 20, 3],\
                 learning_rate = 0.001,\
                 l2_reg = 0.001,\
                 activation_f = tf.nn.elu,\
                 tie_weights = [False, False, False],\
                 root_logdir = '/tmp/',\
                 name = None):
        
        n_outputs = n_inputs
        n_hidden1 = n_hidden[0]
        n_hidden2 = n_hidden[1]
        n_hidden3 = n_hidden[2]
        
        tie_weights4 = tie_weights[0]
        tie_weights5 = tie_weights[1]
        tie_weights6 = tie_weights[2]
        
        # Naive way to check if an iterable of length 6 or a single object is passed to activation_f
        try:
            if(len(activation_f)!=6):
                print('Warning, expected 6 activation functions but received',len(activation_f),\
                      '. Setting tf.nn.elu on all hidden layers and linear output layer.')
                activation_f = [tf.nn.elu] * 5 + [None]
        except:
            activation_f = [activation_f] * 5 + [None]
        
        g = tf.Graph()
        with g.as_default():

            X = tf.placeholder(dtype=tf.float32, shape=(None,n_inputs), name='X')
            
            regularizer = tf.contrib.layers.l2_regularizer(l2_reg)
            initializer = tf.contrib.layers.variance_scaling_initializer()

            ## DEF weights initializer ( variance_scaling_initializer )
            weights1_init = initializer([n_inputs, n_hidden1])
            weights2_init = initializer([n_hidden1, n_hidden2])
            weights3_init = initializer([n_hidden2, n_hidden3])
            if(not tie_weights4):
                weights4_init = initializer([n_hidden3, n_hidden2])
            if(not tie_weights5):
                weights5_init = initializer([n_hidden2, n_hidden1])
            if(not tie_weights6):
                weights6_init = initializer([n_hidden1, n_outputs])

            ## DEF weights
            weights1 = tf.Variable(weights1_init, name='weights1')
            weights2 = tf.Variable(weights2_init, name='weights2')
            weights3 = tf.Variable(weights3_init, name='weights3')
            if (tie_weights4):
                weights4 = tf.transpose(weights3, name='weights4')
            else:
                weights4 = tf.Variable(weights4_init, name='weights4')
            if (tie_weights5):
                weights5 = tf.transpose(weights2, name='weights5')
            else:
                weights5 = tf.Variable(weights5_init, name='weights5')
            if (tie_weights6):
                weights6 = tf.transpose(weights1, name='weights6')
            else:
                weights6 = tf.Variable(weights6_init, name='weights6')

            ## BIASES
            bias1 = tf.Variable(tf.zeros([n_hidden1]), name='bias1')
            bias2 = tf.Variable(tf.zeros([n_hidden2]), name='bias2')
            bias3 = tf.Variable(tf.zeros([n_hidden3]), name='bias3')
            bias4 = tf.Variable(tf.zeros([n_hidden2]), name='bias4')
            bias5 = tf.Variable(tf.zeros([n_hidden1]), name='bias5')
            bias6 = tf.Variable(tf.zeros([n_outputs]), name='bias6')

            ## LAYERS
            hidden1 = neuron_layer(X, weights1, bias1, 'hidden1', activation=activation_f[0])
            hidden2 = neuron_layer(hidden1, weights2, bias2, 'hidden2', activation=activation_f[1])
            hidden3 = neuron_layer(hidden2, weights3, bias3, 'hidden3', activation=activation_f[2])
            hidden4 = neuron_layer(hidden3, weights4, bias4, 'hidden4', activation=activation_f[3])
            hidden5 = neuron_layer(hidden4, weights5, bias5, 'hidden5', activation=activation_f[4])
            outputs = neuron_layer(hidden5, weights6, bias6, 'outputs', activation=activation_f[5])

            with tf.name_scope("loss"):
                error = outputs - X
                reconstruction_loss = tf.reduce_mean(tf.square(error), name="reconstruction_loss")
                reg_loss = regularizer(weights1) + regularizer(weights2)
                loss = reconstruction_loss + reg_loss
                loss = tf.identity(loss, name="total_loss")

            ## TRAINING PHASES

            # Global training
            with tf.name_scope('train'):
                optimizer = tf.train.AdamOptimizer(learning_rate)
                training_op = optimizer.minimize(loss, name='training_op')

            # External layers (1,2 - 5,6)
            with tf.name_scope('phase1'):
                phase1_h3 = neuron_layer(hidden2, weights5, bias5, 'phase1_h3', activation=None)
                phase1_output = neuron_layer(phase1_h3, weights6, bias6, 'phase1_output', activation=None)
                phase1_loss = tf.reduce_mean(tf.square(X - phase1_output)) + regularizer(weights1) + regularizer(weights2)
                phase1_training_op = optimizer.minimize(phase1_loss, name='phase1_training_op')


            # Coding layer (3-4)
            with tf.name_scope('phase2'):
                phase2_loss = tf.reduce_mean(tf.square(hidden4 - hidden2)) + regularizer(weights3)
                if (tie_weights4):
                    train_vars_phase2 = [weights3, bias3, bias4]
                else:
                    phase2_loss = phase2_loss + regularizer(weights4)
                    train_vars_phase2 = [weights3, weights4, bias3, bias4]
                phase2_training_op = optimizer.minimize(phase2_loss, var_list= train_vars_phase2, name='phase2_training_op')

            # External layer (1-6)
            with tf.name_scope('phase1_a'):
                phase1_a_output = neuron_layer(hidden1, weights6, bias6, 'phase1_a_output', activation=None)
                phase1_a_loss = tf.reduce_mean(tf.square(X - phase1_a_output)) + regularizer(weights1)
                phase1_a_training_op = optimizer.minimize(phase1_a_loss, name='phase1_a_training_op')

            # Middle layer (2-5)
            with tf.name_scope('phase1_b'):
                phase1_b_output = neuron_layer(hidden2, weights5, bias5, 'phase1_b_output', activation=None)
                phase1_b_loss = tf.reduce_mean(tf.square(hidden1 - phase1_b_output)) + regularizer(weights2)
                if (tie_weights5):
                    train_vars_phase1_b = [weights2, bias2, bias5]
                else:
                    phase1_b_loss = phase1_b_loss + regularizer(weights5)
                    train_vars_phase1_b = [weights2, bias2, weights5, bias5]
                phase1_b_training_op = optimizer.minimize(phase1_b_loss, var_list= train_vars_phase1_b,\
                                                          name='phase1_b_training_op')

            tf.train.Saver(name = 'saver')
            
            ### ATTRIBUTES
            # Store model and its properties
            self.graph = g
            self.learning_rate = learning_rate
            self.l2_reg = l2_reg
            self.layers = (n_inputs, n_hidden1, n_hidden2, n_hidden3)
            self.tie_weights = (tie_weights6, tie_weights5, tie_weights4)
            self.activation_f = activation_f
            # Name and directory
            if name is None:
                self.name = str(n_hidden1)+('t' if tie_weights6 else '')+'_'+\
                            str(n_hidden2)+('t' if tie_weights5 else '')+'_'+str(n_hidden3)+('t' if tie_weights4 else '')
                for f in activation_f:
                    if f is not None:
                        self.name += '_'+str(f.__name__)
                    else:
                        self.name += '_'+'None'
                self.name += '_'+str(learning_rate)+'_'+str(l2_reg)
            else:
                self.name = name
            self.creation_time = datetime.utcnow().strftime("%d-%m-%y_%H-%M-%S")
            self.root_logdir = root_logdir + self.name +'/'+ self.creation_time +'/'
            # Initialize training history variables
            self.training_pahses_error = []
            self.training_pahses = []
            self.training_pahses_epochs = []
            self.training_pahses_BS = []
            self.step = 0
            self.training_time = []

    def __del__(self):
        print ('Model', self.name,' destructed.')

    def train(self, inputs,\
                     n_epochs = [5, 5, 5, 5, 10],\
                     order = ('1_a', '1_b', '1', '2', 'g'),\
                     batch_size = 150,\
                     summary_step = 0):
        

        n_epoch = {'1_a':n_epochs[0],\
                   '1_b':n_epochs[1],\
                   '1':n_epochs[2],\
                   '2':n_epochs[3],\
                   'g':n_epochs[4]}

        train_op = {'1_a':'phase1_a/phase1_a_training_op',\
                   '1_b':'phase1_b/phase1_b_training_op',\
                   '1':'phase1/phase1_training_op',\
                   '2':'phase2/phase2_training_op',\
                   'g':'train/training_op'}

        n_batches = inputs.shape[0] // batch_size
        
        with tf.Session(graph = self.graph) as sess:
            
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            # If the model was already trained load the weights
            if hasattr(self, 'save_path'):
                saver.restore(sess, self.save_path)
            
            print("Initial error: ", sess.run(sess.graph.get_tensor_by_name('loss/reconstruction_loss:0'),\
                                                     feed_dict={'X:0': inputs}))
            reconstruction_loss_summary = tf.summary.scalar('Rec_loss',\
                                                            sess.graph.get_tensor_by_name('loss/reconstruction_loss:0'))
            file_writer = tf.summary.FileWriter(self.root_logdir, sess.graph)

            
            for phase in order:
                start = time()
                
                self.training_pahses.append(phase)
                self.training_pahses_epochs.append(n_epoch[phase])
                self.training_pahses_BS.append(batch_size)
                # In phases 1_b and 2 the first layers are frozen. Thus we can store their outputs
                # to avoid repeated computations and improve the performances.
                phase_batch=[]
                if (phase=='1_b'):
                    feed_tensor = 'hidden1/output:0'
                    phase_inputs = sess.run(sess.graph.get_tensor_by_name(feed_tensor),\
                                            feed_dict={'X:0': inputs})

                if (phase=='2'):
                    feed_tensor = 'hidden2/output:0'
                    phase_inputs = sess.run(sess.graph.get_tensor_by_name(feed_tensor),\
                                            feed_dict={'X:0': inputs})

                else:
                    phase_inputs = inputs
                    feed_tensor = 'X:0'

                for epoch in range(n_epoch[phase]):

#                     print(phase, epoch)
                    for iteration in range(n_batches):
                        self.step += 1
                        # batch computation (can be simplified)
                        phase_batch = phase_inputs[batch_size * iteration :\
                                                   min(batch_size * (iteration + 1), len(phase_inputs))]
                        # evaluate the phase's training operation
                        sess.run(sess.graph.get_operation_by_name(train_op[phase]),\
                                 feed_dict = {feed_tensor: phase_batch})
                        # Everery summary_step add a summary to file_writer
                        if ( summary_step > 0 and self.step % summary_step == 0 ):

                            X_batch = inputs[batch_size * iteration :\
                                             min(batch_size * (iteration + 1), len(inputs))]
                            summary_str = reconstruction_loss_summary.eval(feed_dict={'X:0': X_batch})
                            file_writer.add_summary(summary_str, self.step)

                phase_error = sess.run(sess.graph.get_tensor_by_name('loss/reconstruction_loss:0'),\
                                          feed_dict={'X:0': inputs})
                print(phase, 'x', n_epoch[phase], " reconstruction error: ", phase_error)
                self.training_pahses_error.append(phase_error)
                self.save_path = saver.save(sess, self.root_logdir+'3MPL-autoencoder.ckpt')
                
                stop = time()
                self.training_time.append(stop - start)
                
                with open(self.root_logdir+'3MPL-autoencoder.history', 'wb') as f:
                    pickle.dump([self.training_pahses_error,\
                                self.training_pahses,\
                                self.training_pahses_epochs,\
                                self.training_pahses_BS,\
                                self.step,\
                                self.training_time], f)
                
            print("Done")
            

        
    def code(self, inputs):
        if not hasattr(self, 'save_path'):
            print(error1)
        else:
            with tf.Session(graph = self.graph) as sess:
                sess.run(tf.global_variables_initializer())
                saver = tf.train.Saver()
                saver.restore(sess, self.save_path)
                return sess.run(sess.graph.get_tensor_by_name('hidden3/output:0'),\
                                     feed_dict={'X:0': inputs})
    
    def decode(self, inputs):
        if not hasattr(self, 'save_path'):
            print(error1)
        else:
            with tf.Session(graph = self.graph) as sess:
                sess.run(tf.global_variables_initializer())
                saver = tf.train.Saver()
                saver.restore(sess, self.save_path)
                return sess.run(sess.graph.get_tensor_by_name('outputs/output:0'),\
                                feed_dict={'hidden3/output:0': inputs})
            
    def reconstruct(self, inputs):
        if not hasattr(self, 'save_path'):
            print(error1)
        else: 
            with tf.Session(graph = self.graph) as sess:
                sess.run(tf.global_variables_initializer())
                saver = tf.train.Saver()
                saver.restore(sess, self.save_path)
                return sess.run(sess.graph.get_tensor_by_name('loss/reconstruction_loss:0'),\
                                     feed_dict={'X:0': inputs})
    
    def load_weights(self, model_root_logdir, deepcopy = True):
        if hasattr(self, 'save_path'):
            print(self.error2)
        else:
            if(deepcopy):
                copytree(model_root_logdir, self.root_logdir)
                self.save_path = self.root_logdir +'3MPL-autoencoder.ckpt'
            else:
                self.root_logdir = model_root_logdir
                self.save_path = model_root_logdir +'3MPL-autoencoder.ckpt'
            try:
                with open(self.root_logdir+'3MPL-autoencoder.history', 'rb') as f:                  
                    self.training_pahses_error,\
                    self.training_pahses,\
                    self.training_pahses_epochs,\
                    self.training_pahses_BS,\
                    self.step,\
                    self.training_time = pickle.load(f)
            except OSError:
                print('Training history not found.')
                
    def info(self, verbose=False):
        print('name:',self.name)
        if verbose:
            print('creation_time:',self.creation_time)
            print('model storage directory:',self.root_logdir)
            print('learning rate:',self.learning_rate)
            print('l2_reg:',self.l2_reg)
            print('layers:',self.layers)
            print('tie_weights:',self.tie_weights)
            activation_str = []
            for f in self.activation_f:
                if f is not None:
                    activation_str.append(str(f.__name__))
                else:
                    activation_str.append('None')
            print('activation_f:', activation_str)
            print('Training History')
            print('Total training time:', sum(self.training_time))
            print('training_pahses:',self.training_pahses)
            print('training_pahses_error:',self.training_pahses_error)
            print('training_pahses_epochs:', self.training_pahses_epochs)
            print('training_time:',self.training_time)
            print('training_pahses_BS:',self.training_pahses_BS)
            print('steps:',self.step)