'''
ID Grid (for various sizes, with various percentage of points considered in the linear fit) from a csv.
3 arguments required:
- csv_dir: csv directory (e.g. '/lustre/MHPC17/pierocor/thesis/data/')
- csv_name: basename of the csv (e.g. '1u-2u_1001ft.csv')
- log_dir: folder where to store the results (e.g. '/lustre/MHPC17/pierocor/thesis/clustering_scripts/data/' )
The csv will alternate columns for the average ID and the std deviation on the average for each percentage. Each row correspond to a different sample size. 
'''
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.linear_model import LinearRegression
import pandas as pd
from time import time
from ID_tools import *
import sys


csv_dir = sys.argv[1]
csv_name = sys.argv[2]
log_dir = sys.argv[3]      #'/u/MHPC17/pierocor/thesis/ID/data/'

csv = csv_dir + csv_name
print("CSV:",csv)
log_file = log_dir + 'GID_mstd_1u-2u_' + csv_name
print("log_file:",log_file)
N_sizes = 16
N_accuracy = 10

data = pd.read_csv(csv, sep='\t')
# data = data.loc[(data['scale_unit'] == 'μm') & ( (data['scale_digit'] == 1) | (data['scale_digit'] == 2) )]
N_features = 2048 if len(data.columns)>2000 else 1001
X = np.unique(data[[str(i) for i in range(N_features)]], axis=0)

x = np.array(np.linspace(0.,1.,N_sizes+1)[1:]*X.shape[0], dtype=int) # Avoid 0
accuracy = np.linspace(0.,1.0,N_accuracy+1)[1:] # Avoid 0
y = np.zeros((len(x),len(accuracy)),dtype=float)
mean_stdev = np.zeros((len(x),len(accuracy)),dtype=float)

for i,size in enumerate(x):
    N_repetition = N_sizes - i
    output = np.zeros((N_repetition,len(accuracy)),dtype=float)
    for j in range(N_repetition):
        output[j,:] = compute_ID(X[np.random.choice(X.shape[0], replace=False, size=size)],\
						      accuracy=accuracy, benchmark = False, algorithm='kd_tree')
    y[i,:] = np.mean(output, axis=0, dtype=float)
    mean_stdev[i,:] = np.std(output, axis=0, dtype=float)/np.sqrt(N_repetition)

df = pd.DataFrame(y, columns= [str("m_{:.3f}".format(acc)) for acc in accuracy])
columns = []
for i,acc in enumerate(accuracy):
    df[str("mstd_{:.3f}".format(acc))] = pd.Series(mean_stdev[:,i])
    columns += [str("m_{:.3f}".format(acc)), str("mstd_{:.3f}".format(acc))]
df['N_samples'] = pd.Series(x)
df = df.set_index('N_samples')                  
df[columns].to_csv(log_file, sep='\t', header=True, float_format='%.3f')
