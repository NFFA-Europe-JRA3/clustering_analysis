import numpy as np
from time import time
from shutil import copytree
from datetime import datetime
from sklearn.neighbors import *
import pickle

def _normalized_ring_density(kneigh_dist):
    densities = np.mean( 1.0/(1e-3 + kneigh_dist[:,kneigh_dist.shape[1]//2:]), axis=1 )
    return  densities/densities.sum()

_density_dict = {'inverse_k_dist': lambda x: 1/x[:,-1],\
                 'GGG': lambda x: np.log(_normalized_ring_density(x)),\
                 'normalized_ring_density': _normalized_ring_density,\
                 'ring_density': lambda x: np.mean(1.0/x[:,x.shape[1]//2:], axis=1),\
                 'disk_density': lambda x: np.mean(1.0/x[:,1:], axis=1)
                }


class DensityPeaks:
    def __init__(self, n_neighbors=-1, density_func='inverse_k_dist',\
                 algorithm='kd_tree', leaf_size=30, metric='minkowski',\
                 p=2, metric_params=None, n_jobs=1):

        try:
            self.density_func = _density_dict[density_func]
            self.density_name = density_func
        except:
            try:
                np.isfinite(density_func(np.arange(0,1,0.1)))
                self.density_func = density_func
            except:
                raise AttributeError('density_func is niether a callable nor a reconized key:'\
                                     +str(_density_dict.keys()))
        self.n_neighbors = n_neighbors
        self.algorithm = algorithm
        self.leaf_size = leaf_size
        self.metric = metric
        self.p = p
        self.metric_params = metric_params
        self.n_jobs = n_jobs
        
    def fit(self, X, y=None):
        """For each data computes its density, the distance to the nearest
        peak, and the index of the nearest peak.
        
        Parameters
        ----------
        X : array-like or sparse matrix, shape (n_samples, n_features)
        
        y : Ignored
        
        Returns
        -------
        density : float numpy array, shape (n_samples,)
        
        nearest_denser_distance : float numpy array, shape (n_samples,)
        
        nearest_denser: int numpy array, shape (n_samples,)
        """
        if (self.n_neighbors < 0):
            n_neighbors = max(int(np.log(3 + X.shape[0])**2), 20)
            n_neighbors = min(n_neighbors, int(np.sqrt(X.shape[0])))
        else:
            n_neighbors = self.n_neighbors
        
        if (np.unique(X,axis=0).shape[0] != X.shape[0] ):
            raise ValueError('There are duplicates in the sample.')
        neigh = NearestNeighbors(n_neighbors = n_neighbors, algorithm = self.algorithm,\
                                 leaf_size=self.leaf_size, metric=self.metric, p=self.p,\
                                 metric_params=self.metric_params, n_jobs = self.n_jobs).fit(X)
        kneigh_dist, kneigh_ind = neigh.kneighbors(X)
        density = self.density_func(kneigh_dist)
        kneigh_density = density[kneigh_ind]

        mask_lower_density = (kneigh_density[:,:] < kneigh_density[:,0:1])
        mask_lower_density |= (kneigh_density[:,:] == kneigh_density[:,0:1]) & ( kneigh_ind[:,:] >= kneigh_ind[:,0:1])

        mask_local_peak = np.all(mask_lower_density, axis=1)
        nearest_denser = kneigh_ind[np.arange(X.shape[0]), mask_lower_density.argmin(axis=1)]
        nearest_denser_distance = kneigh_dist[np.arange(X.shape[0]), mask_lower_density.argmin(axis=1)]

        N_peaks = np.sum(mask_local_peak)
        peaks_global_index = np.arange(X.shape[0])[mask_local_peak]
        
        print('X\t', X.shape)
        print('local_peaks', X[mask_local_peak,:].shape)
        lower_peak = np.argmin(density[mask_local_peak])
        mask_high_density = density > density[lower_peak]
        X_high_density = X[mask_high_density,:]
        high_density_global_index = np.arange(X.shape[0])[mask_high_density]
        print('not_isolated', X_high_density.shape)

        peak_neigh = NearestNeighbors(n_neighbors = X_high_density.shape[0],\
                                      algorithm = self.algorithm,\
                                      leaf_size=self.leaf_size, metric=self.metric,\
                                      p=self.p, metric_params=self.metric_params,\
                                      n_jobs = self.n_jobs).fit(X_high_density)
        peak_dist, peak_ind = peak_neigh.kneighbors(X[mask_local_peak,:])
        peak_density = density[mask_high_density][peak_ind]

        max_peak = np.argmax(peak_density[:,0])

        mask_lower_peak = (peak_density[:,:] < peak_density[:,0:1])
        mask_lower_peak |= (peak_density[:,:] == peak_density[:,0:1]) & ( peak_ind[:,:] >= peak_ind[:,0:1])

        nearest_denser[mask_local_peak] = high_density_global_index[peak_ind[np.arange(N_peaks),\
                                                                             mask_lower_peak.argmin(axis=1)]]

        nearest_denser_distance[mask_local_peak] = peak_dist[np.arange(N_peaks), mask_lower_peak.argmin(axis=1)]
        nearest_denser_distance[peaks_global_index[max_peak]] = peak_dist[max_peak,-1]
        
        self.density = density
        self.nearest_denser_distance = nearest_denser_distance
        self.nearest_denser = nearest_denser
        return density, nearest_denser_distance, nearest_denser
  
    def predict(self, n_clusters):
        """Computes the labels of the fitted data.
        
        Parameters
        ----------
        n_cluster : integer, number of cluster to search for.
        
        Returns
        -------
        labels : labels of the fitted data
        
        cluster_centers : int numpy array, shape (n_samples,). Indices of the cluster centers
        """
        decision_function = self.density * self.nearest_denser_distance
        self.cluster_centers_set = set(decision_function.argsort()[-n_clusters:])
        self.labels_ = np.arange(self.density.shape[0])
        for i in self.density.argsort()[::-1]:
            if i not in self.cluster_centers_set:
                self.labels_[i] = self.labels_[self.nearest_denser[i]]
        return self.labels_, self.cluster_centers_set
    
    def __del__(self):
        print ('DensityPeaks model destructed.')

