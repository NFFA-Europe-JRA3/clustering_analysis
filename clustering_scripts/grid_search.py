'''
Grid test on the usage of different training phases, locked/unlocked weights, and different layers. 
Are 1a, 1b, 2 really usefull? My guess, they increase the speed of the global training.
Does locking the weights really improve perfomances?
Which is the best combination of the three layers?
3 arguments required:
- csv: csv of the input dataset e.g. /lustre/MHPC17/pierocor/thesis/data/1u-2u_2048ft.csv
- tf_log_dir: directory where to store the history and checkpoints of the models
- log_file: path where to store the results of the grid search.

'''
import os
import numpy as np
import tensorflow as tf
import pandas as pd
from time import time
from sklearn.model_selection import train_test_split
from sklearn import metrics
from scipy.cluster.hierarchy import *
from collections import Counter
from autoencoder_3layer import *

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--csv", required=True, help='csv of the input dataset e.g. /lustre/MHPC17/pierocor/thesis/data/1u-2u_2048ft.csv')
parser.add_argument("--tf_log_dir", required=True, help='directory where to store the history and checkpoints of the models, e.g. /lustre/MHPC17/pierocor/thesis/tf_logs/best_NN_2048/.')
parser.add_argument("--log_file", required=True, help='path where to store the results of the grid search.')
args = parser.parse_args()

# PATH 
root = args.tf_log_dir
log_file = args.log_file
csv = args.csv


# Load data
start = time()
images_flat = pd.read_csv(csv, sep='\t')
X_ = images_flat.loc[(images_flat['scale_unit'] == 'μm') & \
                     ( (images_flat['scale_digit'] == 1) | (images_flat['scale_digit'] == 2) )].reset_index()

# THE NUMBER OF INPUTS IS 2048 or 1001
N_input = 2048 if len(data.columns)>2000 else 1001

print(X_.info(verbose=False))
X_train, X_test = train_test_split(X_[[str(i) for i in range(N_input)]], test_size=0.2)
print("X_train:", X_train.shape)
print("X_test:", X_test.shape)
stop = time()
print('Data loaded in',stop-start,'seconds.')
N_unique = len(np.unique(X_[[str(i) for i in range(N_input)]].values, axis=0))
print('Number of unique inference outputs: ', N_unique)

# mask
mask_1u = ((X_['scale_unit'] == 'μm') & (X_['scale_digit'] == 1))
mask_dt1_1u = ((X_['nffa_category']!=-1) & mask_1u)

# info on number of cluster to search for
min_k = 5 #at least 1 -> min split in 2 clusters
max_k = 100
threshold_k = 20

# model variables
n_hidden3 = 13
n_epochs_phase1_a = 5
n_epochs_phase1_b = 5
n_epochs_phase1 = 2
n_epochs_phase2 = 5
n_epochs_global = 20
# learning_rate = 0.001
l2_reg = 0.0001
batch_size = 300
summary_step = 10 

# HEADER
f = open(log_file, 'a')
f.write('# root: '+ root +'\n# CSV:'+ csv +'\n# N_input / n_hidden3:'+ str(N_input) + '/' + str(n_hidden3)\
        + '\n# Epochs per phase (1_a, 1_b, 1, 2, g): (' \
       +str(n_epochs_phase1_a)+', '+str(n_epochs_phase1_b)+', '+str(n_epochs_phase1)+', '\
       +str(n_epochs_phase2)+', '+str(n_epochs_global)+')\n# Batch size:' + str(batch_size)\
       +'\n# K checked in (' + str(min_k) +', '+str(max_k)+'), threshold: '+ str(threshold_k)\
      +'\n# Name\tTest_Error\tTraining_Time\tSparsity\tSil_sc-dt1\tSil_sc-dt1_1u\tK\tMI\tSil_sc-1u\tPhases\tBS'+'\n')
f.close()
# Grid
for learning_rate in [0.001]:
    for activation_f in [tf.nn.elu]:
    # 		 [tf.nn.elu,tf.nn.elu,tf.sigmoid,tf.nn.elu,tf.nn.elu,None], [tf.nn.elu,tf.tanh,tf.tanh,tf.tanh,tf.nn.elu,None]
        for phase_list in [('1_a', '1_b', '2','g'), ('g','g')]:
            for n_hidden1 in [600, 500, 400, 300, 150]:
                for n_hidden2 in [200, 150, 100, 50]:
                    for tie_weights6, tie_weights5, tie_weights4 in [(False, False, False), (True, False, False)]:
                        # Model creation
                        model = autoencoder_3MPL(n_inputs = X_train.shape[1],\
                                                 n_hidden1 = n_hidden1, n_hidden2 = n_hidden2, n_hidden3 = n_hidden3,\
                                                 learning_rate = learning_rate, l2_reg = l2_reg,\
                                                 activation_f = activation_f,\
                                                 tie_weights4 = tie_weights4, tie_weights5 = tie_weights5,\
                                                 tie_weights6 = tie_weights6, root_logdir = root)
                        print('Model created:', model.name)
                        with open(log_file, 'a') as f:
                            f.write(model.name+'  \t')
                            # Model training
                            start = time()
                            model.train(inputs = X_train,\
                                             n_epochs_phase1_a = n_epochs_phase1_a,\
                                             n_epochs_phase1_b = n_epochs_phase1_b,\
                                             n_epochs_phase1 = n_epochs_phase1,\
                                             n_epochs_phase2 = n_epochs_phase2,\
                                             n_epochs_global = n_epochs_global,\
                                             order = phase_list,\
                                             batch_size = batch_size,\
                                             summary_step = summary_step)
                            stop = time()
                            print("Training done in", stop - start, "seconds")
                            test_error = model.evaluate(inputs = X_test)
                            print("Test error:", test_error)
                            f.write("{0:.3f}".format(test_error)+'\t'+ "{0:.1f}".format(sum(model.training_time))+'\t')
                            model.info(verbose=True)
                            # Dimensional reduction (coding)
                            columns_names = ['x'+str(i) for i in range(n_hidden3)]
                            coded = pd.DataFrame(model.code(X_[[str(i) for i in range(2048)]]), columns=columns_names)
                            sparsity = len(np.unique(coded[columns_names].values, axis=0)) / N_unique
                            print("Model Sparsity:", sparsity)
                            f.write("{0:.3f}".format(sparsity)+'  \t')
                            if (sparsity > 0.01):
                                coded = coded.join(X_[['scale_digit', 'scale_unit', 'nffa_category']])
                                # silhouette score of the ground truth
                                sc_dt1 = metrics.silhouette_score(coded[columns_names].loc[coded['nffa_category']!=-1],\
                                                                  coded['nffa_category'].loc[coded['nffa_category']!=-1],\
                                                                  metric='euclidean')
                                f.write("{0:.3f}".format(sc_dt1)+'\t')
                                sc_1u = metrics.silhouette_score(coded[columns_names].loc[mask_dt1_1u], \
                                                                 coded['nffa_category'].loc[mask_dt1_1u],\
                                                                 metric='euclidean')
                                f.write("{0:.3f}".format(sc_1u)+'\t')
                                print("SC: {0:.3f}".format(sc_dt1))
                                print("SC 1 μm: {0:.3f}".format(sc_1u))
                                # Restict to images with scale 1 um
                                small_codec = coded.loc[mask_1u]
                                if (len(np.unique(coded[columns_names].values, axis=0)) > 1):
                                    # Hierachical Clustering by Ward distance
                                    start = time()
                                    Z = linkage(small_codec[columns_names], 'ward')
                                    end = time()
                                    print("Linkage done in ", end-start," sec.")
                                    # Search for optimal k
                                    start = time()
                                    last = Z[-max_k:-min_k - 1, 2]
                                    acceleration = np.diff(last, 2)[::-1]  # 2nd derivative of the distances
                                    k_criterion = acceleration * [1.-min_k/i for i in np.arange(min_k + 1, len(last) + min_k - 1)]
                                    top_k = np.argsort(acceleration) + 1 + min_k 
                                    print ("Top 10 k:", top_k[:-10:-1])
                                    top_rated_k = np.argsort(k_criterion) + 1 + min_k
                                    for k in top_rated_k[::-1]:
                                        if (k > threshold_k):
                                            k_cl_1u = k
                                            break
                                    print ("Top rated k:", top_rated_k[:-10:-1], "\tK:", k_cl_1u)
                                    f.write(str(k_cl_1u)+'\t')
                                    # Label for K Cluster
                                    clusters = fcluster(Z, k_cl_1u, criterion='maxclust') - 1
                                    keys = list(Counter(clusters).keys())
                                    values = list(Counter(clusters).values())
                                    sorted_indices = np.argsort(values)
                                    print([(keys[i], values[i]) for i in sorted_indices[::-1]])
                                    # Mutual Information and silhouette score for optimal k
                                    mi_cl_1u = metrics.normalized_mutual_info_score(\
                                                             small_codec['nffa_category'].loc[small_codec['nffa_category']!=-1],\
                                                             clusters[small_codec['nffa_category']!=-1])
                                    print("MI: ",mi_cl_1u)
                                    f.write("{0:.3f}".format(mi_cl_1u)+'\t')
                                    sc_cl_1u = metrics.silhouette_score(small_codec[columns_names], clusters, metric='euclidean')
                                    print("Silhouette Coefficient ", sc_cl_1u)
                                    f.write("{0:.3f}".format(sc_cl_1u)+'\t')

                                    # Store additional training data                    
                                    f.write(str(model.training_pahses)+'\t'+str(model.training_pahses_BS[0]))
                            f.write('\n')
