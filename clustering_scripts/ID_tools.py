import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.linear_model import LinearRegression
import pandas as pd
from time import time

def compute_ID (X, accuracy = 0.9, return_x_y = False, benchmark=False, algorithm='kd_tree'):
    '''
    Computes the Intrinsic Dimension of the data in X.
    Parameters:
        X = array like object (n_samples, n_features)
        accuracy = fraction of the point used in the linear fit, scalar or array-like object
        return_x_y = if True returns the coordinates of the points used in the fit (without considering the accuracy)
        benchmark = if True returns the time of computing the NearestNeighbors and the LinearRegression
        algorithm = same as NearestNeighbors parameter (‘ball_tree’, ‘kd_tree’, ‘brute’, ‘auto’)
    Returns:
        ID = array of floats shape (len(accuracy),),
        [x, y, time_NN, time_LR] = [np.array (n_samples,), np.array (n_samples,), float, float]
    '''
    if (X.shape[0] != np.unique(X,axis=1).shape[0]):
        print(X.shape,' Not unique.. reshaping:')
        X = np.unique(X,axis=1)
    N = len(X)
    
    if benchmark: start=time()
    nbrs = NearestNeighbors(n_neighbors=3, algorithm=algorithm, n_jobs=-1).fit(X)
    nn_distances, nn_indices = nbrs.kneighbors(X)
    if benchmark: time_NN = time() - start
    
    mu = nn_distances[:,2] / nn_distances[:,1]
    i_sorted = np.argsort(mu)
    F_emp = np.zeros(N, dtype=float)
    F_emp[i_sorted] = [i/N for i in range(N)]
    # Accuracy
    accuracy = np.array(accuracy, dtype=float).reshape(-1,)
    if ( np.any((accuracy>1.0) | (accuracy<0.0)) ):
        raise NameError('Accuracy out of bound! '+str(accuracy[(accuracy>1.0) | (accuracy<0.0)]))
    
    
    res = np.zeros_like(accuracy)
    x = np.log(mu[i_sorted])
    y = -np.log(1. - F_emp[i_sorted])
    
    try:
        if benchmark: start=time()
        model = LinearRegression(fit_intercept=False, n_jobs=1)
        for i,e in enumerate(accuracy):
            N_sample = int(e*N)
            l = model.fit(x[:N_sample].reshape(N_sample,1),y[:N_sample].reshape(N_sample,1))
            res[i] = l.coef_[0,0]
        if benchmark: time_LR = time() - start
    
    except:
        if np.any(np.isnan(x)):
            print('x nan')
        if not np.any(np.isfinite(x)):
            print('x not finite')
        if np.any(np.isnan(y)):
            print('y nan')
        if not np.any(np.isfinite(y)):
            print('y not finite')
    else:
        if return_x_y:
            if benchmark: return res, x, y, time_NN, time_LR
            return res, x, y
        if benchmark: return res, time_NN, time_LR
        return res