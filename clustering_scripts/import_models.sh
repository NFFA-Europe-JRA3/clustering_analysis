### This script is exporting the inference graph of incv3 and froze it with Cristiano's checkpoint
### REQUIRES tensorflow and slim 
### References: https://github.com/tensorflow/models/tree/master/research/slim

#! /bin/bash

TENSORFLOW=${HOME}/miniconda3/envs/myenv/lib/python3.6/site-packages/tensorflow
SLIM=${HOME}/workspace/models/research/slim/
OUTPUT_DIR=${HOME}/thesis/models/
CKPT=/lustre/exact/cdenobi/tf_events_checkpts/nffa_slim_incv3/train-on-nffa_lr0.01-exp-dec_wd0.0001_ep10_bs32/checkpoints/model.ckpt-237953

## exporting inference graph 

python ${SLIM}export_inference_graph.py --alsologtostderr --model_name=inception_v3 --output_file=${OUTPUT_DIR}inception_v3_inf_graph.pb


## Freezing the exported graph

cd ${TENSORFLOW}


## bazel build tensorflow/python/tools:freeze_graph

bazel-bin/tensorflow/python/tools/freeze_graph --input_graph=${OUTPUT_DIR}inception_v3_inf_graph.pb --input_checkpoint=${CKPT} --input_binary=true --output_graph=${OUTPUT_DIR}inception_v3_frozen_train-on-nffa_lr0.01-exp-dec_wd0.0001_ep10_bs32.pb --output_node_names=InceptionV3/Predictions/Reshape_1


### to import the model to Tensorboard

#  python ${HOME}/miniconda3/pkgs/tensorflow-base-1.9.0-gpu_py36h6ecc378_0/lib/python3.6/site-packages/tensorflow/python/tools/import_pb_to_tensorboard.py --model_dir ${OUTPUT_DIR}inception_v3_frozen_train-on-nffa_lr0.01-exp-dec_wd0.0001_ep10_bs32.pb --log_dir ${HOME}/tmp/tf_logs/inception_v3_frozen_train-on-nffa_lr0.01-exp-dec_wd0.0001_ep10_bs32/ 
# tensorboard --logdir=/u/MHPC17/pierocor/tmp/tf_logs/inception_v3_frozen_train-on-nffa_lr0.01-exp-dec_wd0.0001_ep10_bs32/ &