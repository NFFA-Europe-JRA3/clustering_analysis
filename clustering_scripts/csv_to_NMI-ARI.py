'''
Computes the NMI of a Hierarchical clustering against the classification in 10 categories.
Inputs:
--csv_path: CSV of the dataset.
--log_dir: Directory where to store the results.
--method: Algorithm to be used ('SINGLE','COMPL','CENTR','WARD','DP').

Uncommenting some lines it stores the Adjusted Rand Index too.
'''
import numpy as np
from sklearn.metrics import normalized_mutual_info_score, adjusted_rand_score, adjusted_mutual_info_score
from time import time
import pandas as pd
import argparse
from scipy.cluster.hierarchy import *
from DensityPeaks import *
import os
import functools


def agg_score(df, N_feat, N_sample, method):
    res_nmi = np.zeros(N_sample - 2)  ## Used to store MI from k = 2 to N_sample - 1 (if k = N_sample, trivial case)
    res_ari = np.zeros(N_sample - 2)  
    k_gt = N_sample
    Z = linkage(df[[str(i) for i in range(N_feat)]].values, method)
    for k in np.arange(df.shape[0]-1, 1,-1, dtype=int):
        label = fcluster(Z, k, criterion='maxclust')[df['nffa_category']!=-1]
        if ( len(np.unique(label)) < k_gt ):
            k_gt = len(np.unique(label))
            res_nmi[k_gt-2] = normalized_mutual_info_score(df['nffa_category'].loc[df['nffa_category']!=-1],\
                                                  label)
#             res_ari[i] = adjusted_rand_score(df['nffa_category'].loc[df['nffa_category']!=-1],\
#                                                   label)
            if (k_gt == 2): break  
    return res_nmi, res_ari


def DensityPeaks_score(df, N_feat, N_sample):
    res_nmi = np.zeros(N_sample - 2) 
    res_ari = np.zeros(N_sample - 2)
    i=0
    DP_model = DensityPeaks(n_neighbors=-1,density_func='ring_density', n_jobs=-1)
    _ = DP_model.fit(df[[str(i) for i in range(N_feat)]].values)
    for k in np.arange(2, df.shape[0], dtype=int):
        cluster_labels, _ = DP_model.predict(k)
        label = cluster_labels[df['nffa_category']!=-1]
        if ( len(np.unique(label)) == i + 2 ):
            res_nmi[i] = normalized_mutual_info_score(df['nffa_category'].loc[df['nffa_category']!=-1],\
                                                  label)
#             res_ari[i] = adjusted_rand_score(df['nffa_category'].loc[df['nffa_category']!=-1],\
#                                                   label)
            i +=1
            if (i == N_sample - 2): break 
    return res_nmi, res_ari


method_dictionary = {'SINGLE': functools.partial(agg_score, method='single'),\
                     'COMPL': functools.partial(agg_score, method='complete'),\
                     'CENTR': functools.partial(agg_score, method='centroid'),\
                     'WARD': functools.partial(agg_score, method='ward'),\
                     'DP': DensityPeaks_score
                     }

def csv_to_score(filename, method):
    df = pd.read_csv(filename, sep='\t')
    N_feat = len(df.columns) - 4  # Select just the columns defining the vectors
    
    _, unique_i = np.unique(df[[str(i) for i in range(N_feat)]],axis=0, return_index=True)
    
    df = df.loc[unique_i]  #  Do not consider duplicates!
    
    N_labelled = np.sum(df['nffa_category']!=-1)
    print('Number of unique labelled data:', N_labelled)
    
    return method_dictionary[method](df, N_feat, N_labelled)


parser = argparse.ArgumentParser()
parser.add_argument("--csv_path", required=True)
parser.add_argument("--log_dir", required=True, help='Path where to store the results: log_dir/')
parser.add_argument("--method", required=True, help='Select one of the following: ' + str(method_dictionary.keys()))
args = parser.parse_args()


csv = args.csv_path
log_dir = args.log_dir
method = args.method

csv_name = os.path.splitext(os.path.basename(csv))[0]

start = time()
NMI, ARI = csv_to_score(csv, method)
print('MI and ARI from', csv,', using ', method,' algorithm, computed in ', time()-start,'seconds')

log_file_nmi = os.path.join(log_dir, '_NMI_'+str(method)+'_'+str(csv_name)+'.dat')
print('Storing NMI in:', log_file_nmi)
with open(log_file_nmi, 'ab') as f:
    NMI.tofile(f)

# log_file_ari = os.path.join(log_dir, '_ARI_'+str(method)+'_'+str(csv_name)+'.dat')
# print('Storing ARI in:', log_file_ari)
# with open(log_file_ari, 'ab') as f:
#     ARI.tofile(f)
    
