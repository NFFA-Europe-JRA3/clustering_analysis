#  Feature Learning and Clustering Analysis for Images Classification
### Author: Piero Coronica
### Thesis: MHPC_thesis.pdf
### Presentation: MHPC_presentation.pdf

All the scripts in this folder were slightly modified before being published. No check was done after the modification thus some errors can occur (mainly because of changes in the path names or wrong parse of the inputs). Use them with care!

## BASH
#### import_models.sh
Script to download SLIM inception v3 pb file and to update it the weights from a frozen checkpoint. It requires Tensorflow and Slim


## Python Modules
#### ID_tools.py
compute_ID(X, accuracy = 0.9, return_x_y = False, benchmark=False, algorithm='kd_tree')
    Computes the Intrinsic Dimension of the data in X.
Usage examples in `ID_synthetic_cases.ipynb` and `4dts_screeplot.ipynb`.

#### autoencoder_3layer.py
Defines the class autoencoder_3MPL implementing a 6 layers autoencoder. Full documentation can be found in chapter 3 of `MHPC_thesis.pdf`.

#### DensityPeaks.py
Defines the class DensityPeaks implementing the Density Peaks algorithm. The design of the model is similar to scikit-learn (once created use first the fit method then the predict).

## Python Scripts
#### supervised_feature_learning.py
Script to load an inception V3 model from a pb file and perform inference till the desired operation (before or after the last fully connected).

#### grid_ID_from_csv-error.py
ID Grid (for various sizes, with various percentage of points considered in the linear fit) from a csv.

#### grid_search.py
Grid search for the best autoencoder parameters. The outputs can be found in `data/grid_search_1u-2u_*`.

#### csv_to_NMI-ARI.py
Computes the NMI of a Hierarchical clustering against the classification in 10 categories. Uncommenting some lines it stores the Adjusted Rand Index too.

## Notebooks
#### heatmaps-output_layers.ipynb
Heatmaps of the distances induced by the features of the last 4 layers of inception V3 (for both models) on sample counting 431 images.

#### PCA.ipynb
Execute the PCA on a representation of the dataset.

#### ID_synthetic_cases.ipynb
Intrinsic Dimension: Thick line example.

#### 4dts_screeplot.ipynb
Intrinsic Dimension: Linear fit of the 4 representations.

#### ID_with_error_bars.ipynb
Represents the outputs of `grid_ID_from_csv-error.py` in different graphs.

#### code.ipynb
Loads an autoencoder and encodes a datasets. It was used to create the smaller representations (e.g. to pass from 1u-2u_2048ft.csv to 1u-2u_2048in_18.csv).

#### Distance_Heatmap.ipynb
Heatmaps of the distances induced by the features learning process. (MIND THE PATHS!)

#### Mutual_information.ipynb
Examples and first graph on the NMI score. 

#### Results.ipynb
NMI scores of the 5 clustering algorithms considered. (MIND THE PATHS!)


